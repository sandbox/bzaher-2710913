<?php

/**
 * @file
 * Contains the definition list style plugin.
 */

/**
 * Style plugin to render each item as a definition list
 *
 * @ingroup views_style_plugins
 */
class views_plugin_style_definition_list_view extends views_plugin_style {
  /**
   * Definition.
   */
  public function option_definition() {
    $options['dl_classes'] = array('default' => '');
    return $options;
  }

  /**
   * Form.
   */
  function options_form(&$form, &$form_state) {
    $form['dl_classes'] = array(
        '#type' => 'textfield',
        '#title' => t('Definition list classes'),
        '#description' => '',
        '#default_value' => $this->options['dl_classes'],
    );
  }
}
