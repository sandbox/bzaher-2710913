<?php
/**
 * @file views-bootstrap-grid-plugin-style.tpl.php
 * Default simple view template to display a definition list
 * 
 * @ingroup views_templates
 */
?><div id="views-definition-list-<?php print $id ?>"<?php if($classes) { print ' class="'.$classes.'"'; } ?>>
  <?php if (!empty($title)) : ?>
    <h3><?php print $title; ?></h3>
  <?php endif; ?>
  <dl <?php if($dl_classes): ?> class="<?php print $dl_classes; ?>"<?php endif; ?>>
    <?php  foreach ($rows as $id => $row): ?>
      <?php print $row; ?>
    <?php endforeach; ?>
  </dl>
</div>
