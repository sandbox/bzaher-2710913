<?php
/**
 * @file
 * Provide views data and handlers.
 */

/**
 * Implements hook_views_plugins().
 */
function views_definition_list_views_plugins() {
  $module_path = drupal_get_path('module', 'views_definition_list');
  return array(
      'style' => array(
          'definition_list_style' => array(
              'title' => t('Definition List'),
              'help' => '',
              'handler' => 'views_plugin_style_definition_list_view',
              'path' => $module_path . '/plugins',
              'theme' => 'views_definition_list_style',
              'theme path' => $module_path . '/templates',
              'theme file' => 'theme.inc',
              'uses options' => TRUE,
              'uses row plugin' => TRUE,
              'use pager' => FALSE,
              'type' => 'normal',
          )
      ),
      'row' => array(
          'definition_list_row' => array(
              'title' => t('Definition List Row'),
              'help' => '',
              'handler' => 'views_plugin_row_definition_list_view',
              'path' => drupal_get_path('module', 'views_definition_list') . '/plugins',
              'theme' => 'views_definition_list_row',
              'theme path' => $module_path . '/templates',
              'theme file' => 'theme.inc',
              'type' => 'normal',
              'uses fields' => TRUE,
          )
      )
  );
}
